# Mapping block form fields to DX8 custom element fields (an example). 

## To test

- Install this module 
- On a node, open the sidebar browser and place an instance of the element `Render a block with fields` on the layout canvas (this will be a new element at the bottom of the light highlighted in pink).
- Once placed on the layout canvas, open the settings for the element and type some text in the `Text content` field and save. 
- Save the node and you'll see your text rendered. 

What's happening here is the custom element is taking the text you input and passing it through as field data to the blocks render function. 

With this technique, it's possible to create a DX8 custom element with form fields that map exactly to the fields in a block. You do not need to place the block in a region for this to work.

The code includes a block plugin and a DX8 custom element plugin. The code is heavily commented and annotated, but any questions please ask.    