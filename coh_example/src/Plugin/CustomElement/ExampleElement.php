<?php

namespace Drupal\coh_example\Plugin\CustomElement;

use Drupal\cohesion_elements\CustomElementPluginBase;
use Drupal\block\Entity\Block;
use Drupal\coh_example\Plugin\Block\ExampleBlock;

/**
 * Simple DX8 custom element plugin that demonstrates passing DX8 custom
 * element field data through to a block render function.
 *
 * @CustomElement(
 *   id = "coh_block_example",
 *   label = @Translation("Render a block with fields")
 * )
 */
class ExampleElement extends CustomElementPluginBase {

  /**
   * This sets up the form for the custom DX8 element.
   *
   * @return array
   */
  public function getFields() {
    return [
      'text' => [
        'htmlClass' => 'col-xs-12',
        'title' => 'Text content',
        'type' => 'textfield',
        'placeholder' => 'e.g. DX8 is great',
      ],
    ];
  }

  /**
   * The render function for the DX8 custom element. Here, we're simply
   * passing the DX8 custom element form field data to the block and
   * rendering it.
   *
   * @param $settings
   * @param $markup
   * @param $class
   *
   * @return array|\Drupal\block\Entity\Block|\Drupal\Core\Entity\EntityInterface|null
   */
  public function render($settings, $markup, $class) {
    // Create an instance of the block without needing to place it in a region.
    /** @var ExampleBlock $block */
    $block = \Drupal::service('plugin.manager.block')->createInstance('coh_example_block');

    if ($block) {
      // Pass the DX8 custom element form field data to the block.
      $block->setConfigurationValue('text', isset($settings['text']) ? $settings['text'] : '');

      // Return the rendered block.
      return $block->build();
    }

  }

}