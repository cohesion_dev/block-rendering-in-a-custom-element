<?php

namespace Drupal\coh_example\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a 'ExampleBlock' block.
 *
 * @Block(
 * id = "coh_example_block",
 * admin_label = @Translation("DX8 example block"),
 * )
 */
class ExampleBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    // Adds a single textfield to the block form.
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Type some text here'),
      '#default_value' => isset($config['text']) ? $config['text'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('text', $form_state->getValue('text'));
  }

  /**
   * The render function for the block. It takes the form data and renders
   * as a simple piece of text.
   *
   * @return array
   */
  public function build() {
    $config = $this->getConfiguration();

    // Get the text from the field.
    $text = !empty($config['text']) ? $config['text'] : '';

    // Render the text.
    return [
      '#markup' => $this->t('Your text is: @text!', ['@text' => $text]),
    ];
  }
}